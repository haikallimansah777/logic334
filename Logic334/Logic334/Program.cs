﻿/*output*/
Console.WriteLine("Hello, World!");
Console.Write("asdsad");
Console.WriteLine();
Console.Write("Masukan nama : ");

/*input*/
string nama = Console.ReadLine();
string kelas = "Batch 334";
Console.WriteLine(nama);
int umur = 23;


/*3 cara mengoutputkan variabel*/
Console.WriteLine("Hi, {0} Selamat Datang! {1} umur : {2}", nama, kelas, umur);
Console.WriteLine("Hi, "+nama+" Selamat Datang! "+kelas+" umur : "+umur+"");
Console.WriteLine($"Hi, {nama} Selamat Datang! {kelas} umur : {umur}");

/*Contoh implisit (kalo belum tau variable nya tipe data apa bisa otomatis menyesuaikan pake var) kebalikannya eksplisit*/
/*var nilaiint = 0; //otomatis int
var nilaistring = "jelek"; //otomatis string
var nilaibool = true; //otomatis bool*/

/*variabel mutable & immutable*/
/*mutable digantikan*/
string mutable = "apa itu";
mutable = "itu apa";
Console.WriteLine(mutable);
/*mutable ditambahkan*/
mutable += " apa yah";
Console.WriteLine(mutable);
/*imutable tidak bisa digantikan*/
const double phi = 3.14;
//double phi = 1;
Console.WriteLine(phi);



Console.ReadKey();