﻿//Tugas4();
//Tugas5();
Tugas6();
Console.ReadKey();

static void Tugas4()
{
    int h, s, p = 0;
    Console.WriteLine("---Program penghitung rokok yang dirangkai & penghasilannya---");
    Console.Write("Masukan jumlah puntung rokok : ");
    int n = int.Parse(Console.ReadLine());

    h = (n / 8);
    if (h > 0)
    {
        Console.WriteLine($"Rokok yang dapat dirangkai sejumlah = {h}");
    }
    else if (h <= 0) 
    {
        Console.WriteLine();
    }
  
    s = (n % 8);
    Console.WriteLine($"Puntung rokok yang tersisa sejumlah = {s}");

    p = (h * 500);
    if (p >= 500) 
    {
        Console.WriteLine($"Penghasilan pemulung tersebut = Rp. {p}");
    }
    else { Console.WriteLine();}

}

static void Tugas5()
{
    Console.WriteLine("---Program grade nilai---");
    Console.Write("Masukan nilai : ");
    int n = int.Parse(Console.ReadLine());

    if (n >= 80 && n <= 100)
    {
        Console.WriteLine("Mendapatkan grade : A");
    }
    else if (n >= 60 && n < 80)
    {
        Console.WriteLine("Mendapatkan grade : B");
    }
    else if (n < 60 && n >= 0)
    {
        Console.WriteLine("Mendapatkan grade : C");
    }
    else
    {
        Console.WriteLine("Nilai tidak sesuai");
    }

    /*    switch (n)
        {
            case (n >= 80):
                Console.WriteLine("Mendapatkan grade : A");
                break;
            case (n >= 60 && n < 80):
                Console.WriteLine("Mendapatkan grade : B");
                break;
            default:
                Console.WriteLine("Mendapatkan grade : C");
                break;
        }*/
}

static void Tugas6()
{
    Console.WriteLine("---Program menentukan bilangan ganjil atau genap---");
    Console.Write("Masukan bilangan : ");
    int n = int.Parse(Console.ReadLine());

    if (n % 2 == 0)
    {
        Console.WriteLine($"Angka {n} adalah genap");
    }
    else { Console.WriteLine($"Angka {n} adalah ganjil"); }
}