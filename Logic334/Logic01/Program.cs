﻿//OperatorAritmatika();
//OperatorPenugasan();
//OperatorPerbandingan();
//OperatorLogika();
//Modulus();
Konversi();
//MethodReturnType();
Console.ReadKey();

/*operator*/
static void OperatorAritmatika()
{
    int mangga, apel, hasil = 0;
    Console.WriteLine("---Operator Aritmatika---");
    Console.Write("Masukan mangga : ");
    mangga = Convert.ToInt32(Console.ReadLine());
    //mangga = int.Parse(Console.Readline());
    Console.Write("Masukan apel : ");
    apel = Convert.ToInt32(Console.ReadLine());

    hasil = mangga + apel;
    Console.WriteLine($"Hasil mangga + apel = {hasil}");

}
/*operator penugasan*/
static void OperatorPenugasan()
{
    int mangga = 10;
    int apel = 8;

    //replace nilai dari variabel
    mangga = 15;

    Console.WriteLine("---Operator Penugasan---");
    Console.Write("Masukan mangga = ");
    mangga = Convert.ToInt32(Console.ReadLine());
    Console.WriteLine($"Mangga = {mangga}");
    Console.Write("Masukan apel = ");
    apel = Convert.ToInt32(Console.ReadLine());

    //operator penugasan
    apel += 6;
    Console.WriteLine($"Apel += 6 = {apel}");

}

static void Modulus()
{
    int mangga, orang, hasil = 0;
    Console.WriteLine("---Operator Aritmatika---");
    Console.Write("Masukan mangga : ");
    mangga = Convert.ToInt32(Console.ReadLine());
    //mangga = int.Parse(Console.Readline());
    Console.Write("Masukan orang : ");
    orang = Convert.ToInt32(Console.ReadLine());

    hasil = mangga % orang;
    Console.WriteLine($"Hasil mangga % orang = {hasil}");
}


/*membuat method*/
static void Konversi()
{
    Console.WriteLine("Contoh konversi");
    int myInt = 10;
    double myDouble = 52.25;
    bool myBool = false;
    
    string strMyInt = Convert.ToString(myInt);
    double doubleMyInt = Convert.ToDouble(myInt);
    int intMyDouble = Convert.ToInt32(myDouble);

    Console.WriteLine(strMyInt);
    Console.WriteLine(doubleMyInt);
    Console.WriteLine(intMyDouble);
    Console.WriteLine(myBool.ToString());
}

/*Operator Perbandingan*/
static void OperatorPerbandingan()
{
    int mangga, apel = 0;
    Console.WriteLine("---Operator Perbandingan---");
    Console.Write("Masukan mangga : ");
    mangga = Convert.ToInt32(Console.ReadLine());
    Console.Write("Masukan apel :");
    apel = Convert.ToInt32(Console.ReadLine());

    Console.WriteLine("Hasil Perbandingan = ");
    Console.WriteLine($"Mangga > Apel : {mangga > apel}");
    Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
    Console.WriteLine($"Mangga < Apel : {mangga < apel}");
    Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
    Console.WriteLine($"Mangga == Apel : {mangga == apel}");
    Console.WriteLine($"Mangga != Apel : {mangga != apel}");
}

/*Operator Logika*/
static void OperatorLogika()
{
    Console.WriteLine("---Operator Logika---");
    Console.Write("Masukan umur anda : ");
    int umur = Convert.ToInt32(Console.ReadLine());
    Console.Write("Masukan kata sandi : ");
    string password = Console.ReadLine();
    bool isAdult = umur > 18;
    bool isPasswordValid = password == "admin";
    if(isAdult && isPasswordValid)
    {
        Console.WriteLine("Selamat datang");
    }
    else
    {
        Console.WriteLine("Coba lagi");
    }
}

/*method*/
static int hasil(int mangga, int apel)
{
    int hasil = 0;
    hasil = mangga + apel;
    return hasil;

}

static void MethodReturnType()
{
    Console.WriteLine("--- Cetak Method ---");
    Console.Write("Masukan mangga : ");
    int mangga = int.Parse(Console.ReadLine());
    Console.Write("Masukan apel : ");
    int apel = int.Parse(Console.ReadLine());

    int jumlah = hasil(mangga, apel);
    Console.WriteLine($"Hasil mangga + apel = {jumlah}");
}