﻿//LoopStatement();
//ForEachLoop();
WhileLoop();
Console.ReadKey();

static void LoopStatement()
{
    for (int i = 0; i < 5; i++)
    {
        Console.WriteLine(i);
    }

}

static void ForEachLoop()
{
    string[] cars = { "Volvo", "BMW", "Ford", "Mazda" };
    foreach (string i in cars)
    {
        Console.WriteLine(i);
    }

}

static void WhileLoop()
{
    int i = 0;
    while (i < 5)
    {
        Console.WriteLine(i);
        i++;
    }
}

