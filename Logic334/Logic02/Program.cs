﻿//IfStatement();
//ElseStatement();
//IfElseIfStatement();
//IfNestedStatement();
//TernaryStatement();
SwitchStatement();
Console.ReadKey();

static void IfStatement()
{
    Console.WriteLine("---Ini adalah if statement---");
    Console.Write("Masukan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan nilai y : ");
    int y = int.Parse(Console.ReadLine());
    
    if (x >= 10)
    {
        Console.WriteLine("x is greater or equal to 10");
    }
    if (y <= 5)
    {
        Console.WriteLine("y is lesser or equal to 5");
    }
    
}

static void ElseStatement()
{
    Console.WriteLine("---Ini adalah else statement---");
    Console.Write("Masukan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    if (x >= 10)
    {
        Console.WriteLine("x is greater than equal to 10");
    }
    else
    {
        Console.WriteLine("x is less than 10");
    }
}

static void IfElseIfStatement()
{
    Console.WriteLine("---Ini adalah if else if statement---");
    Console.Write("Masukan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    if (x == 10)
    {
        Console.WriteLine("x value is equal to 10");
    }
    else if (x > 10)
    {
        Console.WriteLine("x value is greater than 10");
    }
    else
    {
        Console.WriteLine("x value is less than 10");
    }
}

static void IfNestedStatement()
{
    Console.WriteLine("---Ini adalah if nested statement---");
    Console.Write("Masukan nilai : ");
    int nilai = int.Parse(Console.ReadLine());
    if (nilai >= 50)
    {
        Console.WriteLine("Kamu berhasil");
        if (nilai == 100)
        {
            Console.WriteLine("Kamu keren");
        }
    }
    else
    {
        Console.WriteLine("Kamu gagal");
    }
}

static void TernaryStatement()
{
    Console.WriteLine("---Ini adalah ternary statement---");
    Console.Write("Masukan nilai x : ");
    int x = int.Parse(Console.ReadLine());
    Console.Write("Masukan nilai y : ");
    int y = int.Parse(Console.ReadLine());

    // if true = '?', else = ':'
    string z = x > y ? "x lebih besar dari y" : x < y ? "x lebih kecil dari y" : "x sama dengan y";
    Console.WriteLine(z);
}

static void SwitchStatement()
{
    Console.WriteLine("---Ini adalah switch statement----");
    Console.Write("Pilih buah kesukaan anda (apel/jeruk/pisang) : ");
    string pilihan = Console.ReadLine().ToLower();

    switch (pilihan)
    {
        case "apel":
            Console.WriteLine("Anda memilih buah apel");
            break;
        case "jeruk":
            Console.WriteLine("Anda memilih jeruk");
            break;
        case "pisang":
            Console.WriteLine("Anda memilih pisang");
            break;
        default:
            Console.WriteLine("Anda memilih buah yang lain");
            break;
    }
    
}